package exchanges

import (
	"bytes"
	"errors"
	"fmt"
	"log"
	"sort"
	"sync"
	"time"

	"github.com/shopspring/decimal"
)

// ExchangeInfo holds information about the exchange and links to useful information.
type ExchangeInfo struct {
	Name           string
	URL            string
	APIDocsURL     string
	APIKeyURL      string
	Blurb          string
	CallsPerSecond int
}

func (e *ExchangeInfo) String() string {
	return e.Name
}

// Exchange should be implemented by every plugin exchange.
// Implementations should convert the requests and responses from the exchange API to native data types.
type Exchange interface {
	Info() *ExchangeInfo
	Quote(pair *Pair) (*Quote, error)
	PairFormat(pair *Pair) string
	Balances() ([]*Balance, error)
	DepositAddress(currency string) (string, error)
	WithdrawTo(currency string, amount decimal.Decimal, address string) error
	TradeFees(amount decimal.Decimal) decimal.Decimal
	WithdrawFees(currency string, amount decimal.Decimal) decimal.Decimal
	Buy(pair *Pair, amount, rate decimal.Decimal) (*Trade, error)
	Sell(pair *Pair, amount, rate decimal.Decimal) (*Trade, error)
}

// Exchanges holds all the exchanges loaded into the program and provides a mutex to protect the in-app memory state.
type Exchanges struct {
	Exchanges []Exchange
	sync.Mutex
}

// CurrentQuotes gets quotes for the pair on all exchanges.
func (xs *Exchanges) CurrentQuotes(pair *Pair) []*Quote {
	var wg sync.WaitGroup
	var quotes []*Quote
	exchangeCount := len(xs.Exchanges)
	wg.Add(exchangeCount)
	for _, exchange := range xs.Exchanges {
		go func(exchange Exchange) {
			defer wg.Done()
			exchangeInfo := exchange.Info()
			log.Printf("Looking up %s on %s", pair, exchangeInfo)
			quote, err := exchange.Quote(pair)
			if err != nil {
				log.Printf("Error retreiving %s from %s: %s", pair, exchangeInfo, err.Error())
				return
			}
			xs.Lock()
			quotes = append(quotes, quote)
			xs.Unlock()
		}(exchange)
	}
	wg.Wait()
	return quotes
}

// SlowestExchange returns the slowest exchange.
// Deprecated since the bot will be switching to websockets.
func (xs *Exchanges) SlowestExchange() Exchange {
	exchanges := xs.Exchanges
	sort.Slice(exchanges, func(i, j int) bool {
		return exchanges[i].Info().CallsPerSecond < exchanges[j].Info().CallsPerSecond
	})
	return exchanges[0]
}

// Balances gets balances on every connected exchange.
func (xs *Exchanges) Balances() Balances {
	var wg sync.WaitGroup
	wg.Add(len(xs.Exchanges))
	balances := Balances{}
	for _, exchange := range xs.Exchanges {
		go func(exchange Exchange) {
			defer wg.Done()
			exchangeBalances, err := exchange.Balances()
			if err != nil {
				log.Println(err.Error())
				return
			}
			xs.Lock()
			balances[exchange.Info().Name] = exchangeBalances
			xs.Unlock()
		}(exchange)
	}

	wg.Wait()
	return balances
}

// Balance is an internal representation of an exchange balance.
type Balance struct {
	Amount   decimal.Decimal
	Currency string
}

func (b *Balance) String() string {
	return fmt.Sprintf("%s - %v", b.Currency, b.Amount)
}

// Balances holds balances on all exchanges.
type Balances map[string][]*Balance

func (b Balances) String() string {
	var buffer bytes.Buffer
	for exchange, balances := range b {
		buffer.WriteString(fmt.Sprintf("%s - %v\n", exchange, balances))
	}
	return buffer.String()
}

// Order is an entry in an exchange's order book.
type Order struct {
	Amount decimal.Decimal
	Rate   decimal.Decimal
}

// Cost is the size of the order.
func (o *Order) Cost() decimal.Decimal {
	return o.Amount.Mul(o.Rate)
}

func (o *Order) String() string {
	return fmt.Sprintf("%v at %v", o.Amount, o.Rate)
}

// Quote is the top of the order book.
type Quote struct {
	Pair      *Pair
	Bid       *Order
	Ask       *Order
	Exchange  Exchange
	Timestamp time.Time
	Fees      decimal.Decimal
}

func (q *Quote) String() string {
	return fmt.Sprintf("%s on %s. Bid: %v %s Ask: %v %s", q.Pair, q.Exchange.Info().Name, q.Bid, q.Pair.Base, q.Ask, q.Pair.Quote)
}

// Pair is a trading pair, like BTC/LTC.
type Pair struct {
	Base  string
	Quote string
}

func (p *Pair) String() string {
	return fmt.Sprintf("%s/%s", p.Base, p.Quote)
}

// Opportunity is an arbitrage opportunity between 2 exchanges.
type Opportunity struct {
	Lower     *Quote
	Higher    *Quote
	Timestamp time.Time
}

// Cost is how much it will cost to execute the arbitrage.
func (o *Opportunity) Cost() decimal.Decimal {
	lowerQuote := o.Lower
	lowerRate := lowerQuote.Ask.Rate
	lowerAmount := lowerQuote.Ask.Amount
	return lowerRate.Mul(lowerAmount)
}

// ProfitPerUnit returns the amount of profit per unit of the coin traded.
func (o *Opportunity) ProfitPerUnit() decimal.Decimal {
	totalFees := o.Higher.Exchange.TradeFees(decimal.NewFromFloat(1)).Add(o.Lower.Exchange.TradeFees(decimal.NewFromFloat(1)))
	priceDifference := o.Higher.Bid.Rate.Sub(o.Lower.Ask.Rate).Sub(totalFees)
	return priceDifference
}

func (o *Opportunity) String() string {
	return fmt.Sprintf(
		"Buy %v on %v: %v\nSell %v on %v: %v\nProfit: %v %s per %s traded",
		o.Lower.Pair,
		o.Lower.Exchange.Info(),
		o.Lower.Ask,
		o.Higher.Pair,
		o.Higher.Exchange.Info(),
		o.Higher.Bid,
		o.ProfitPerUnit(),
		o.Higher.Pair.Base,
		o.Higher.Pair.Quote,
	)
}

// ProfitableWithin will determine if a trade is profitable enough to execute.
func (o *Opportunity) ProfitableWithin(threshold decimal.Decimal) bool {
	return o.ProfitPerUnit().GreaterThan(threshold)
}

// Execute performs the trade on both exchanges.
// TODO: Make concurrent.
func (o *Opportunity) Execute() (*Arbitrage, error) {
	var wg sync.WaitGroup
	wg.Add(2)
	lowerQuote := o.Lower
	lowerExchange := lowerQuote.Exchange
	arbitrageAmount := lowerQuote.Ask.Amount
	lowerTrade, err := lowerExchange.Buy(lowerQuote.Pair, arbitrageAmount, lowerQuote.Ask.Rate)
	if err != nil {
		return nil, err
	}

	higherQuote := o.Higher
	higherExchange := higherQuote.Exchange
	higherTrade, err := higherExchange.Sell(higherQuote.Pair, higherQuote.Bid.Rate, arbitrageAmount)
	if err != nil {
		return nil, err
	}
	return &Arbitrage{BuyTrade: lowerTrade, SellTrade: higherTrade}, nil
}

// NewOpportunity finds an arbitrage opportunity in a list of quotes.
func NewOpportunity(subscription *Subscription, quotes []*Quote) (*Opportunity, error) {
	if len(quotes) < 2 {
		return nil, errors.New("not enough Exchanges are up")
	}
	sort.Slice(quotes, func(i, j int) bool {
		return quotes[i].Ask.Rate.LessThan(quotes[j].Bid.Rate)
	})

	lower := quotes[0]
	higher := quotes[len(quotes)-1]
	opportunity := &Opportunity{
		Lower:     lower,
		Higher:    higher,
		Timestamp: time.Now(),
	}
	if opportunity.ProfitableWithin(subscription.ProfitThreshold) {
		return opportunity, nil
	}
	return nil, fmt.Errorf("No profitable arbitrage opportunities on road right now for %s.", lower.Pair)
}

// Subscription holds bookkeeping information about the operator.
type Subscription struct {
	Username        string
	Pair            *Pair
	Timestamp       time.Time
	ProfitAddress   string
	ProfitThreshold decimal.Decimal
}
