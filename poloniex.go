package main

import (
	"fmt"
	"time"

	"github.com/joncooperworks/ArbitrageBot/exchanges"
	"github.com/pharrisee/poloniex-api"
	"github.com/shopspring/decimal"
)

type PoloniexExchange struct {
	api *poloniex.Poloniex
}

func (p PoloniexExchange) Info() *exchanges.ExchangeInfo {
	return &exchanges.ExchangeInfo{
		Name:           "Poloniex",
		URL:            "https://poloniex.com/",
		APIDocsURL:     "https://poloniex.com/support/api/",
		APIKeyURL:      "https://poloniex.com/apiKeys",
		CallsPerSecond: 6,
	}
}

func (p PoloniexExchange) PairFormat(pair *exchanges.Pair) string {
	return fmt.Sprintf("%s_%s", pair.Base, pair.Quote)
}

func (p PoloniexExchange) Quote(pair *exchanges.Pair) (*exchanges.Quote, error) {
	orderBook, err := p.api.OrderBook(p.PairFormat(pair))
	if err != nil {
		return nil, err
	}
	if len(orderBook.Asks) < 1 || len(orderBook.Bids) < 1 {
		return nil, fmt.Errorf("Poloniex order book not deep enough for %s", pair)
	}

	bid, ask := orderBook.Bids[0], orderBook.Asks[0]
	return &exchanges.Quote{
		Bid:       &exchanges.Order{Rate: bid.Rate, Amount: bid.Amount},
		Ask:       &exchanges.Order{Rate: ask.Rate, Amount: ask.Amount},
		Timestamp: time.Now(),
		Exchange:  p,
		Pair:      pair,
	}, nil
}

func (p PoloniexExchange) Balances() ([]*exchanges.Balance, error) {
	poloniexBalances, err := p.api.Balances()
	if err != nil {
		return nil, err
	}
	balances := []*exchanges.Balance{}
	for currency, poloniexBalance := range poloniexBalances {
		balance := &exchanges.Balance{
			Amount:   poloniexBalance.Available,
			Currency: currency,
		}
		balances = append(balances, balance)
	}
	return balances, nil
}

func (p PoloniexExchange) DepositAddress(currency string) (string, error) {
	depositAddress, err := p.api.GenerateNewAddress(currency)
	if err != nil {
		return "", nil
	}
	return depositAddress, nil
}

func (p PoloniexExchange) WithdrawTo(currency string, amount decimal.Decimal, address string) error {
	withdrawalResponse, err := p.api.Withdraw(currency, amount, address)
	if err != nil {
		return err
	}

	if withdrawalResponse.Success != 1 {
		return fmt.Errorf("Error withdrawing %v %s to %s", amount, currency, address)
	}
	return nil
}

func (p PoloniexExchange) WithdrawFees(currency string, amount decimal.Decimal) decimal.Decimal {
	return decimal.NewFromFloat(0)
}

func (p PoloniexExchange) TradeFees(amount decimal.Decimal) decimal.Decimal {
	return decimal.NewFromFloat(0.0025).Mul(amount)
}

func (p PoloniexExchange) Buy(pair *exchanges.Pair, amount, rate decimal.Decimal) (*exchanges.Trade, error) {
	buyResponse, err := p.api.Buy(p.PairFormat(pair), amount, rate)
	if err != nil {
		return nil, err
	}

	return exchanges.NewTrade(exchanges.Buy, string(buyResponse.OrderNumber), pair, &exchanges.Order{Amount: amount, Rate: rate}, p), nil
}

func (p PoloniexExchange) Sell(pair *exchanges.Pair, amount, rate decimal.Decimal) (*exchanges.Trade, error) {
	sellResponse, err := p.api.Sell(p.PairFormat(pair), amount, rate)
	if err != nil {
		return nil, err
	}

	return exchanges.NewTrade(exchanges.Sell, string(sellResponse.OrderNumber), pair, &exchanges.Order{Amount: amount, Rate: rate}, p), nil
}

func NewPoloniexExchange(apiKey, apiSecret string) PoloniexExchange {
	return PoloniexExchange{
		api: poloniex.NewWithCredentials(apiKey, apiSecret),
	}
}
