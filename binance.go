package main

import (
	"errors"
	"fmt"
	"time"

	"github.com/joncooperworks/ArbitrageBot/exchanges"
	"github.com/pdepip/go-binance/binance"
	"github.com/shopspring/decimal"
)

// BinanceExchange contains bindings for https://binance.com
type BinanceExchange struct {
	api *binance.Binance
}

// Info returns useful information about Binance.
func (b BinanceExchange) Info() *exchanges.ExchangeInfo {
	return &exchanges.ExchangeInfo{
		Name:           "Binance",
		URL:            "https://binance.com",
		APIDocsURL:     "https://github.com/binance-exchange/go-binance/",
		APIKeyURL:      "https://binance.com",
		CallsPerSecond: 2,
	}
}

// PairFormat converts a Pair struct to Binance's QUOTE_BASE format.
func (b BinanceExchange) PairFormat(pair *exchanges.Pair) string {
	return fmt.Sprintf("%s%s", pair.Quote, pair.Base)
}

// Quote fetches the top of the order book for a pair.
func (b BinanceExchange) Quote(pair *exchanges.Pair) (*exchanges.Quote, error) {
	quote, err := b.api.GetOrderBook(binance.OrderBookQuery{
		Symbol: b.PairFormat(pair),
		Limit:  5,
	})
	if err != nil {
		return nil, err
	}
	if len(quote.Bids) < 1 || len(quote.Asks) < 1 {
		return nil, errors.New("not enough depth on binance for pair")
	}

	return &exchanges.Quote{
		Pair:      pair,
		Bid:       convertOrder(quote.Bids[0]),
		Ask:       convertOrder(quote.Asks[0]),
		Exchange:  b,
		Timestamp: time.Now(),
	}, nil
}

// Balances fetches our coin balances from Binance.
func (b BinanceExchange) Balances() ([]*exchanges.Balance, error) {
	return nil, nil
}

// DepositAddress gets our deposit address.
func (b BinanceExchange) DepositAddress(currency string) (string, error) {
	return "", errors.New("deposit address not supported by API bindings")
}

// WithdrawTo issues a withdrawal request to a given coin address.
func (b BinanceExchange) WithdrawTo(currency string, amount decimal.Decimal, address string) error {
	return errors.New("withdrawal not supported by API bindings ")
}

// WithdrawFees calculates the withdrawal fees given a currency.
func (b BinanceExchange) WithdrawFees(currency string, amount decimal.Decimal) decimal.Decimal {
	return decimal.NewFromFloat(0)
}

// TradeFees returns the fees per trade on Binance.
func (b BinanceExchange) TradeFees(amount decimal.Decimal) decimal.Decimal {
	return decimal.NewFromFloat(0.0025).Mul(amount)
}

// Buy sends a buy order to Binance.
func (b BinanceExchange) Buy(pair *exchanges.Pair, amount, rate decimal.Decimal) (*exchanges.Trade, error) {
	return nil, errors.New("the dev hasn't built this yet")
}

// Sell sends a sell order to Binance.
func (b BinanceExchange) Sell(pair *exchanges.Pair, amount, rate decimal.Decimal) (*exchanges.Trade, error) {
	return nil, errors.New("the dev hasn't built this yet")
}

// NewBinanceExchange initializes the bindings with API key and Secret.
func NewBinanceExchange(apiKey, apiSecret string) BinanceExchange {
	return BinanceExchange{
		api: binance.New(apiKey, apiSecret),
	}
}

func convertOrder(order binance.Order) *exchanges.Order {
	return &exchanges.Order{
		Amount: decimal.NewFromFloat(order.Quantity),
		Rate:   decimal.NewFromFloat(order.Price),
	}
}
