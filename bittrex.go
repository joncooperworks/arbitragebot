package main

import (
	"fmt"
	"log"
	"time"

	"github.com/joncooperworks/ArbitrageBot/exchanges"
	"github.com/shopspring/decimal"
	"github.com/toorop/go-bittrex"
)

type BittrexExchange struct {
	api *bittrex.Bittrex
}

func (b BittrexExchange) Info() *exchanges.ExchangeInfo {
	return &exchanges.ExchangeInfo{
		Name:           "Bittrex",
		URL:            "https://bittrex.com",
		APIDocsURL:     "https://godoc.org/github.com/toorop/go-bittrex",
		APIKeyURL:      "https://bittrex.com/Manage#sectionApi",
		CallsPerSecond: 2,
	}
}

func (b BittrexExchange) PairFormat(pair *exchanges.Pair) string {
	return fmt.Sprintf("%s-%s", pair.Base, pair.Quote)
}

func (b BittrexExchange) Quote(pair *exchanges.Pair) (*exchanges.Quote, error) {
	bittrexPair := b.PairFormat(pair)
	orderBook, err := b.api.GetOrderBook(bittrexPair, "both", 1)
	if err != nil {
		return nil, err
	}

	if len(orderBook.Buy) < 1 || len(orderBook.Sell) < 1 {
		return nil, fmt.Errorf("Bittrex order book not deep enough for %s\n", bittrexPair)
	}

	bid, ask := orderBook.Buy[0], orderBook.Sell[0]
	return &exchanges.Quote{
		Bid:       &exchanges.Order{Rate: bid.Rate, Amount: bid.Quantity},
		Ask:       &exchanges.Order{Rate: ask.Rate, Amount: ask.Quantity},
		Timestamp: time.Now(),
		Exchange:  b,
		Pair:      pair,
	}, nil
}

func (b BittrexExchange) Balances() ([]*exchanges.Balance, error) {
	bittrexBalances, err := b.api.GetBalances()
	if err != nil {
		return nil, err
	}

	balances := []*exchanges.Balance{}
	for _, bittrexBalance := range bittrexBalances {
		balance := &exchanges.Balance{
			Amount:   decimal.NewFromFloat(bittrexBalance.Available),
			Currency: bittrexBalance.Currency,
		}
		balances = append(balances, balance)
	}
	return balances, nil
}

func (b BittrexExchange) DepositAddress(currency string) (string, error) {
	depositAddress, err := b.api.GetDepositAddress(currency)
	if err != nil {
		return "", nil
	}
	return depositAddress.Address, nil
}

func (b BittrexExchange) WithdrawTo(currency string, amount decimal.Decimal, address string) error {
	floatAmount, exact := amount.Float64()
	if !exact {
		log.Printf("Inexact float conversion for withdrawal of %v %s to %s\n", amount, currency, address)
	}
	withdrawalUUID, err := b.api.Withdraw(address, currency, floatAmount)
	if err != nil {
		return err
	}

	log.Printf("Bittrex withdrawal uuid: %s\n", withdrawalUUID)
	return nil
}

func (b BittrexExchange) WithdrawFees(currency string, amount decimal.Decimal) decimal.Decimal {
	return decimal.NewFromFloat(0)
}

func (b BittrexExchange) TradeFees(amount decimal.Decimal) decimal.Decimal {
	return decimal.NewFromFloat(0.0025).Mul(amount)
}

func (b BittrexExchange) Buy(pair *exchanges.Pair, amount, rate decimal.Decimal) (*exchanges.Trade, error) {
	floatRate, precise := rate.Float64()
	if !precise {
		log.Printf("Loss of precision when converting rate for %v\n", pair)
	}
	floatAmount, precise := amount.Float64()
	if !precise {
		log.Printf("Loss of precision when converting amount for %v\n", pair)
	}
	orderUUID, err := b.api.BuyLimit(b.PairFormat(pair), floatAmount, floatRate)
	if err != nil {
		return nil, err
	}

	return exchanges.NewTrade(exchanges.Buy, orderUUID, pair, &exchanges.Order{Amount: amount, Rate: rate}, b), nil
}

func (b BittrexExchange) Sell(pair *exchanges.Pair, amount, rate decimal.Decimal) (*exchanges.Trade, error) {
	floatRate, precise := rate.Float64()
	if !precise {
		log.Printf("Loss of precision when converting rate for %v\n", pair)
	}
	floatAmount, precise := amount.Float64()
	if !precise {
		log.Printf("Loss of precision when converting amount for %v\n", pair)
	}
	orderUUID, err := b.api.SellLimit(b.PairFormat(pair), floatAmount, floatRate)
	if err != nil {
		return nil, err
	}
	return exchanges.NewTrade(exchanges.Sell, orderUUID, pair, &exchanges.Order{Amount: amount, Rate: rate}, b), nil
}

func NewBittrexExchange(apiKey, apiSecret string) BittrexExchange {
	return BittrexExchange{
		api: bittrex.New(apiKey, apiSecret),
	}
}
