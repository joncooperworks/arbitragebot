package main

import (
	"fmt"
	"log"
	"net/http"
	_ "net/http/pprof"
	"time"

	"github.com/joncooperworks/ArbitrageBot/exchanges"
	"github.com/marcsauter/single"
	"github.com/reactivex/rxgo/observable"
	"github.com/reactivex/rxgo/observer"
	"github.com/shopspring/decimal"
)

// OperatorSubscription returns the operator's subscription to this bot.
// For now, everything is hardcoded.
func OperatorSubscription() *exchanges.Subscription {
	return &exchanges.Subscription{
		Username:        "coopycoop",
		Pair:            &exchanges.Pair{Base: "BTC", Quote: "LTC"},
		Timestamp:       time.Now(),
		ProfitAddress:   "1CFV6J7Qd99GD9GsSJdCbwAmduWtMPPK1Z7",
		ProfitThreshold: decimal.NewFromFloat(0),
	}
}

// SupportedExchanges loads the exchanges this bot can trade on.
func SupportedExchanges() *exchanges.Exchanges {
	// TODO: Load exchange plugins from disk.
	exchanges := exchanges.Exchanges{
		Exchanges: []exchanges.Exchange{
			NewBittrexExchange("a8576c5396804029a029f05c27c9969b", "db93c815f9da4251a3ea42a1b59675ac"),
			NewPoloniexExchange("T9VS8PKH-E4AH8RLN-7G8F6EAX-RZLLAIYU", "6d7ff5d8ebf35e46747211afab23b6797f36ccc7eddd75918814be74cc57dfb4db47a5d9ae4555b0d5c144ecfda8e7b0fc983aba842c9be504d986116236973a"),
			NewBinanceExchange("BFwzqq75uxfmmcm5HKVgD8cTvtMHVJIM3qyaH1OxvDu2pkMQkDzPqPMzU2f1Nbhk", "yzwq5VHDl0QFO8RPVPJrRfnHChN0sQY0Wh0uQo9I8XRaHTNlA60mqUfa94mFR715"),
		},
	}
	return &exchanges
}

func takeOpportunitiesWorker(opportunities <-chan *exchanges.Opportunity) {
	total := decimal.NewFromFloat(0)
	seen := map[string]bool{}
	count := 0
	for opportunity := range opportunities {
		log.Printf("Opportunity Detected:\n%v\n", opportunity)
		log.Printf("Buying the top of the sell queue costs %v %s.", opportunity.Cost(), opportunity.Lower.Pair.Base)
		tradeAmount := decimal.NewFromFloat(0.5)
		_, found := seen[opportunity.String()]
		if !found {
			seen[opportunity.String()] = true
			total = total.Add(opportunity.ProfitPerUnit())
			count++
		}
		fmt.Printf("Total profit so far: %v %s, %d %s traded\n", total, opportunity.Lower.Pair.Base, count, opportunity.Higher.Pair.Base)
		buyOrder := func() interface{} {
			buyTrade, err := opportunity.Lower.Exchange.Buy(opportunity.Lower.Pair, tradeAmount, opportunity.Lower.Ask.Rate)
			if err != nil {
				return err
			}
			return buyTrade
		}
		sellOrder := func() interface{} {
			sellTrade, err := opportunity.Higher.Exchange.Sell(opportunity.Higher.Pair, tradeAmount, opportunity.Higher.Bid.Rate)
			if err != nil {
				return err
			}
			return sellTrade
		}

		watcher := observer.Observer{

			// Register a handler function for every next available item.
			NextHandler: func(item interface{}) {
				trade := item.(*exchanges.Trade)
				log.Printf("Trade made: %s", trade)
			},

			// Register a handler for any emitted error.
			ErrHandler: func(err error) {
				log.Printf("Encountered error: %v\n", err)
			},

			// Register a handler when a stream is completed.
			DoneHandler: func() {
				log.Println("Done!")
			},
		}
		source := observable.Start(buyOrder, sellOrder)
		source.Subscribe(watcher)
	}
}

// Only one subscription allowed per instance of the bot to avoid API rate limiting issues.
var (
	subscription = OperatorSubscription()
)

func main() {
	s := single.Single{}
	s.Lock()
	defer s.Unlock()
	log.Println("Arbitrage Sniper by https://telegram.me/coopycoop.")
	log.Println("This is a high-risk tool. Do not stake more than you can afford to lose.")
	xs := SupportedExchanges()
	slowestExchangeRate := xs.SlowestExchange().Info().CallsPerSecond
	requestCooldown := slowestExchangeRate
	log.Print(xs.Balances())
	interval := time.Second / time.Duration(requestCooldown)
	log.Printf("Checking prices every %v", interval)
	ticker := time.NewTicker(interval)
	opportunities := make(chan *exchanges.Opportunity)
	go func() {
		fmt.Println(http.ListenAndServe("localhost:6060", nil))
	}()
	go takeOpportunitiesWorker(opportunities)
	for range ticker.C {
		quotes := xs.CurrentQuotes(subscription.Pair)
		for _, quote := range quotes {
			log.Println(quote.String())
		}
		opportunity, err := exchanges.NewOpportunity(subscription, quotes)
		if err != nil {
			log.Println(err.Error())
		} else {
			opportunities <- opportunity

		}
	}
}
