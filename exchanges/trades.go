package exchanges

import (
	"fmt"
	"time"

	"github.com/shopspring/decimal"
)

const (
	// Buy order
	Buy = "BUY"
	// Sell order
	Sell = "SELL"
)

// Trade is an internal representation of a trade on all exchanges.
type Trade struct {
	TradeType       string
	Exchange        Exchange
	Pair            *Pair
	Timestamp       time.Time
	Order           *Order
	ExchangeReceipt string
}

func (t *Trade) String() string {
	return fmt.Sprintf("%s %s on %v at %v. Trade ID: %s", t.TradeType, t.Pair, t.Exchange.Info().Name, t.Timestamp, t.ExchangeReceipt)
}

// Total cost of order including fees.
func (t *Trade) Total() decimal.Decimal {
	orderAmountBeforeFees := t.Order.Rate.Mul(t.Order.Amount)
	total := t.Exchange.TradeFees(orderAmountBeforeFees).Add(orderAmountBeforeFees)
	return total
}

// Arbitrage trade.
// Holds both buy and sell trades.
type Arbitrage struct {
	BuyTrade  *Trade
	SellTrade *Trade
}

// Profit returns the total profit from a trade after fees.
func (a *Arbitrage) Profit() decimal.Decimal {
	salePrice := a.SellTrade.Total()
	costPrice := a.BuyTrade.Total()
	return salePrice.Sub(costPrice)
}

// NewTrade returns a trade object from the exchange's order.
func NewTrade(tradeType, exchangeReceipt string, pair *Pair, order *Order, exchange Exchange) *Trade {
	switch tradeType {
	case Buy:
	case Sell:
	default:
		panic(fmt.Sprintf("Invalid trade type %s", tradeType))

	}

	return &Trade{
		Timestamp:       time.Now(),
		ExchangeReceipt: exchangeReceipt,
		Order:           order,
		TradeType:       tradeType,
		Pair:            pair,
		Exchange:        exchange,
	}
}
